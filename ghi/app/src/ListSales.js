import { useEffect, useState } from 'react';

function SalesList() {
    const [sales, setSaleList]= useState([]);

    const fetchData = async () => {
        const listUrl = "http://localhost:8090/api/sales/"
        const response = await fetch(listUrl);

        if (response.ok) {
            const data = await response.json();
            setSaleList(data.sales);
        }
    };
    useEffect(() => {
        fetchData();
      }, []);
return(
    <div>
        <div>
            <h1>Sales</h1>
        </div>
        <div>
            <table>
                <thead>
                    <tr>
                        <th>Salesperson Employee ID</th>
                        <th>Salesperson's Name</th>
                        <th>Customer's Name </th>
                        <th>VIN</th>
                        <th>Price </th>
                    </tr>
                </thead>
                <tbody>
                    {sales.map((sale)=> {
                        return (
                            <tr key={sale.id}>
                                <td>{sale.salesperson.employee_id}</td>
                                <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                                <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                                <td>{sale.automobile.vin}</td>
                                <td>{sale.price}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    </div>
)
}

export default SalesList
