import React, { useEffect, useState } from 'react';

function ManufacturerForm() {
    useEffect(() => {
        fetchData();
    }, []);
    const [manufacturers, setManufacturers] = useState([])
    const fetchData = async () => {
        const url = 'http://localhost:8100/api/manufacturers/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setState(data.manufacturers);

        }
    }
    const [name, setName] = useState([]);
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.name = name;

    const manufacturerUrl = 'http://localhost:8100/api/manufacturers/';
    const fetchConfig = {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
    };

    const response = await fetch(manufacturerUrl, fetchConfig);
    if (response.ok) {
        const newManufacturer = await response.json();
        setName('');
        return (
            <>
                <form onSubmit={handleSubmit} id="create-manufacturer-form">
                    <input onChange={handleNameChange} placeholder="Name" required
                        type="text" name="roomCount" id="roomCount"
                        className="form-control" value={name} />

                    <button type="submit" className="btn btn-primary mb-2">Submit</button>
                </form>
            </>
        )
    }
}
}

    export default ManufacturerForm
