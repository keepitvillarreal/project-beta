# CarCar

Team:

* Person 1 - Beau Bullitt: Car Service app
* Person 2 - Jose Villareal: Car Sales app

## Design

## Service microservice

Explain your models and integration with the inventory
microservice, here:
- Service microservice will have a poller that gets the data from the Automobile model in
- the Inventory app and assigns that data to a value, AutomobileVO, inside the service app.

## Sales microservice

Explain your models and integration with the inventory
microservice, here.
- sales will poll for automobile to create a value called automobileVO.
- automobileVo will be a foreign key in a sales model.
