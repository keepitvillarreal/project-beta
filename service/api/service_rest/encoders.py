from common.json import ModelEncoder
from .models import Technician, Appointment, AutomobileVO


class TechniciansListEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id",
    ]


class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "datetime",
        "reason",
        "status",
        "vin",
        "customer",
        "is_Vip",
        "technician",
    ]
    encoders = {
        "technician": TechniciansListEncoder(),
    }


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "id",
        "import_id",
        "import_href",
        "color",
        "year",
        "sold",
        "vin",
    ]
