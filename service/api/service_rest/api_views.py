from .models import Technician, Appointment, AutomobileVO
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .encoders import (
    TechniciansListEncoder,
    AppointmentListEncoder,
    AutomobileVOEncoder,
)

@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechniciansListEncoder,
        )
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechniciansListEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_technician(request, id):
    if request.method == "GET":
        technician = Technician.objects.get(id=id)
        return JsonResponse(
            technician,
            encoder=TechniciansListEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Technician.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Technician.objects.filter(id=id).update(**content)
        technician = Technician.objects.get(id=id)
        return JsonResponse(
            technician,
            encoder=TechniciansListEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        automobile_vins = []
        auto_vos_all = AutomobileVO.objects.all()
        try:
            if auto_vos_all != None:
                for auto in auto_vos_all:
                    automobile_vins.append(auto.vin)
                for appointment in appointments:
                    if appointment.vin in automobile_vins:
                        appointment.is_Vip = "True"
                    else:
                        appointment.is_VIP = "False"
        except auto_vos_all is None:
            print("No Automobiles to cross check VIP status")
        return JsonResponse(
            {"appointments": appointments}, encoder=AppointmentListEncoder, safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            if "technician" in content:
                technician_id = content["technician"]
                technician = Technician.objects.get(id=technician_id)
                content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Technician id"},
                status=400,
            )
        appointment = Appointment.objects.create(**content)
        return JsonResponse(appointment, encoder=AppointmentListEncoder, safe=False)


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_appointment(request, id):
    if request.method == "GET":
        appointment = Appointment.objects.get(id=id)
        return JsonResponse(appointment, encoder=AppointmentListEncoder, safe=False)
    elif request.method == "DELETE":
        count, _ = Appointment.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    elif request.method == "PUT":
        content = json.loads(request.body)
        try:
            if "technician" in content:
                technician_id = content["technician"]
                technician = Technician.objects.get(id=technician_id)
                content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Technician id"},
                status=400,
            )
        Appointment.objects.filter(id=id).update(**content)
        appointment = Appointment.objects.get(id=id)
        return JsonResponse(appointment, encoder=AppointmentListEncoder, safe=False)


@require_http_methods(["GET"])
def api_history_appointments(request):
    appointments_finished = Appointment.objects.filter(status="finished")
    appointments_canceled = Appointment.objects.filter(status="canceled")
    # merge operator merges cancelled and finished dicts into one
    appointments_history = appointments_finished | appointments_canceled
    automobile_vins = []
    auto_vos_all = AutomobileVO.objects.all()
    try:
        if auto_vos_all != None:
            for auto in auto_vos_all:
                automobile_vins.append(auto.vin)
            for appointment in appointments_history:
                if appointment.vin in automobile_vins:
                    appointment.is_Vip = "True"
                else:
                    appointment.is_VIP = "False"
    except auto_vos_all is None:
        print("No Automobiles to cross check VIP status")
    return JsonResponse(
        appointments_history, encoder=AppointmentListEncoder, safe=False
    )


@require_http_methods(["GET"])
def api_history_vin_filter(request, vin_id):
    appointments_finished = Appointment.objects.filter(status="finished")
    appointments_canceled = Appointment.objects.filter(status="canceled")
    # merge operator merges cancelled and finished dicts into one
    appointments_history = appointments_finished | appointments_canceled
    appointment_history_vin_filter = appointments_history.filter(vin=vin_id)
    return JsonResponse(
        appointment_history_vin_filter, encoder=AppointmentListEncoder, safe=False
    )


@require_http_methods(["GET"])
def api_list_automobilevos(request):
    automobile_vos = AutomobileVO.objects.all()
    return JsonResponse(
        {"automobile_vos": automobile_vos},
        encoder=AutomobileVOEncoder,
    )


@require_http_methods(["DELETE"])
def api_show_automobilevos(request, id):
    count, _ = AutomobileVO.objects.filter(id=id).delete()
    return JsonResponse({"deleted": count > 0})
