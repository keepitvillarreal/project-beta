from django.urls import path
from .api_views import (
    api_list_technicians,
    api_show_technician,
    api_list_appointments,
    api_show_appointment,
    api_list_automobilevos,
    api_show_automobilevos,
    api_history_appointments,
    api_history_vin_filter,
)

urlpatterns = [
    path("technicians/", api_list_technicians, name="list_technicians"),
    path("technicians/<int:id>/", api_show_technician, name="show_technician"),
    path("appointments/", api_list_appointments, name="list_appointments"),
    path("appointments/<int:id>/", api_show_appointment, name="show_appointment"),
    path("appointments/history/", api_history_appointments, name="appointment_history"),
    path("appointments/history/<vin_id>/", api_history_vin_filter, name="appointment_history_vin_filter"),
    path("automobilevos/", api_list_automobilevos, name="list_automobile_vos"),
    path("automobilevos/<int:id>/", api_show_automobilevos, name="show_automobile_vos"),
]
