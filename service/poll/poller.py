import django
import os
import sys
import time
import json
import requests
from service_rest.models import AutomobileVO

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "service_project.settings")
django.setup()


def get_automobiles():
    url = "http://inventory-api:8000/api/automobiles/"
    response = requests.get(url)
    content = json.loads(response.content)

    for car in content["autos"]:
        AutomobileVO.objects.update_or_create(
            import_href=car["href"],
            import_id=car["id"],
            color=car["color"],
            year=car["year"],
            sold=car["sold"],
            defaults={"vin": car["vin"]},
        )


def poll(repeat=True):
    while True:
        try:
            get_automobiles()
        except Exception as e:
            print(e, file=sys.stderr)
        if not repeat:
            break
        time.sleep(60)


if __name__ == "__main__":
    poll()
