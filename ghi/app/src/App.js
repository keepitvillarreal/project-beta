import { BrowserRouter, Routes, Route, } from 'react-router-dom';
import React, { useEffect} from 'react';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturerList from './ListManufacturer';
import SalespersonList from './ListSalesperson';
import CustomerList from './ListCustomers';
import SalesList from './ListSales';
import TechniciansList from './TechniciansList';
import loadData from '.';
import AppointmentList from './AppointmentsList';
import AppointmentHistory from './AppointmentHistory';
import VehicleModels from './VehicleModels';
import AutomobileList from './ListAutomobile';
import AutosSale from './ListUnsold';

function App(props) {
  useEffect(() => {
    loadData();
}, []);
  return (
    <BrowserRouter>
      <Nav/>
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers">
            <Route path="list" element={<ManufacturerList/>} />
          </Route>
          <Route path="automobiles">
            <Route path="list" element={<AutomobileList/>} />
          </Route>
          <Route path="salespeople">
            <Route path="list" element={<SalespersonList/>} />
          </Route>
          <Route path="customer">
            <Route path="list" element={<CustomerList/>} />
          </Route>
          <Route path="sales">
            <Route path ="list" element={<SalesList/>} />
            <Route path ="unsold" element={<AutosSale/>} />
          </Route>
          <Route path="technicians/" element={<TechniciansList technicians={props.technicians}/>} />
          <Route path="appointments/" element={<AppointmentList appointments={props.appointments}/>} />
          <Route path="appointments/history/" element={<AppointmentHistory/>} />
          <Route path="vehicle_models/" element={<VehicleModels vehicles={props.vehicles}/>}/>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
