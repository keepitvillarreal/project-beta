import React, { useEffect, useState } from 'react';

function VehicleModelForm() {
    useEffect(() => {
        fetchData();
    }, []);
    const [vehicleModels, setVehicleModels] = useState([])
    const fetchData = async () => {
        const url = 'http://localhost:8100/api/models/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setState(data.models)
}
    }
}

export default VehicleModelForm;
