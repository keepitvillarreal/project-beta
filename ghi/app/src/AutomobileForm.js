import React, { useEffect, useState } from 'react';

function AutomobileForm() {
    useEffect(() => {
        fetchData();
    }, []);
    const [automobiles, setAutomobiles] = useState([]);
    const fetchData = async () => {
        const url = 'http://localhost:8000/api/states/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setState(data.autos);

}
    }
}

export default AutomobileForm;
