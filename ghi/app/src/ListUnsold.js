import { useEffect, useState } from 'react';

function AutosSale() {

    const [sales, setAutoSale]= useState([]);

    const fetchData = async () => {
        const listUrl = "http://localhost:8090/api/sales/automobiles/"
        const response = await fetch(listUrl);

        if (response.ok) {
            const data = await response.json();
            setAutoSale(data);
        }
    };
    useEffect(() => {
        fetchData();
      }, []);
return(
    <div>
        <div>
            <h1>Unsold Cars</h1>
        </div>
        <div>
            <table>
                <thead>
                    <tr>
                        <th>VIN</th>
                    </tr>
                </thead>
                <tbody>
                    {sales.map((sale)=> {
                        return (
                            <tr key={sale.id}>
                                <td>{sale.vin}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    </div>
)
}

export default AutosSale
