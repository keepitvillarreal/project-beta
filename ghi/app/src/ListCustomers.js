import { useEffect, useState } from 'react';
function CustomerList(){
    const [customer, setCustomerList] = useState([]);

    const fetchData = async () => {
        const listUrl = "http://localhost:8090/api/customers/"
        const response = await fetch(listUrl);

        if(response.ok){
            const data = await response.json();
            setCustomerList(data)
        }
    };

    useEffect(() => {
        fetchData();
      }, []);

return(
    <div>
        <div>
            <h1>Customer List</h1>
        </div>
        <div>
            <table>
                <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Phone Number</th>
                        <th>Address</th>
                    </tr>
                </thead>
                <tbody>
                    {customer.map((customer)=> {
                        return (
                            <tr key={customer.id}>
                                <td>{customer.first_name}</td>
                                <td>{customer.last_name}</td>
                                <td>{customer.phone_number}</td>
                                <td>{customer.address}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    </div>
)
}

export default CustomerList
