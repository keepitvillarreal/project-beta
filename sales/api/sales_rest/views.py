from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from .models import Saleperson, Customer, AutomobileVO, Sale
import json
from django.http import JsonResponse
from .encoders import (
    AutomobileVOEncoder,
    SalespersonListEncoder,
    CustomerListEncoder,
    SaleEncoder,
)

require_http_methods(["GET", "POST"])
def api_list_salesperson(request):
    if request.method == "GET":
        salesperson = Saleperson.objects.all()
        return JsonResponse(
            {"salesperson": salesperson}, encoder=SalespersonListEncoder
        )
    else:
        try:
            content = json.loads(request.body)
            salesperson = Saleperson.objects.create(**content)
            return JsonResponse(salesperson, encoder=SalespersonListEncoder, safe=False)

        except:
            response = JsonResponse({"Message": "Salesperons could not be created"})
            response.status_code = 400
            return response


require_http_methods(["GET", "DELETE"])
def api_show_salesperson(request, id):
    if request.method == "GET":
        try:
            salesperson = Saleperson.objects.get(employee_id=id)
            return JsonResponse(salesperson, encoder=SalespersonListEncoder, safe=False)
        except Saleperson.DoesNotExist:
            response = JsonResponse({"Message": "This Salesperson does not exist"})
            response.status_code = 400
            return response
    elif request.method == "DELETE":
        try:
            salesperson = Saleperson.objects.get(employee_id=id)
            salesperson.delete()
            return JsonResponse({"Deleted": "Salesperson has been deleted"})

        except Saleperson.DoesNotExist:
            return JsonResponse(
                {"Message": "Salesperson does not exist therefor nothing was deleted"}
            )


require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(customers, encoder=CustomerListEncoder, safe=False)
    else:
        try:
            content = json.loads(request.body)
            customers = Customer.objects.create(**content)
            return JsonResponse(customers, encoder=CustomerListEncoder, safe=False)
        except:
            response = JsonResponse({"Message": "Customer could not be created"})
            response.status_code = 400
            return response


require_http_methods(["GET", "DELETE"])
def api_show_customer(request, id):
    if request.method == "GET":
        try:
            customer = Customer.objects.get(id=id)
            return JsonResponse(customer, encoder=CustomerListEncoder, safe=False)
        except Customer.DoesNotExist:
            response = JsonResponse(
                {"Message": "Error, customer requested does not exist"}
            )
            response.status_code = 400
            return response
    elif request.method == "DELETE":
        try:
            customer = Customer.objects.get(id=id)
            customer.delete()
            return JsonResponse({"Deleted": "Customer has been deleted"})
        except Customer.DoesNotExist:
            return JsonResponse(
                {"Message": "Customer does not exist, therefore nothing was deleted"}
            )


@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    if request.method == "POST":
        content = json.loads(request.body)
        try:
            salesperson = Saleperson.objects.get(employee_id=content["salesperson"])
            content["salesperson"] = salesperson
            customer = Customer.objects.get(id=content["customer"])
            content["customer"] = customer
            automobile = AutomobileVO.objects.get(vin=content["automobile"])
            if automobile.sold == True:
                response = JsonResponse(
                    {"Message": "This vehicle has already been sold."}, status=404
                )
                return response
            content["automobile"] = automobile
            print("THIS IS MY CONTENT", content)
            automobile.sold = True
            automobile.save()
            sale = Sale.objects.create(**content)
            return JsonResponse(sale, encoder=SaleEncoder, safe=False)
        except Saleperson.DoesNotExist:
            response = JsonResponse(
                {"message": "Salesperson does not exist."}, status=404
            )
            return response

    else:
        if request.method == "GET":
            sales = Sale.objects.all()
            return JsonResponse({"sales": sales}, encoder=SaleEncoder, safe=False)


require_http_methods(["GET", "DELETE"])
def api_show_sale(request, id):
    if request.method == "GET":
        try:
            sale = Sale.objects.get(id=id)
            return JsonResponse(sale, encoder=SaleEncoder, safe=False)
        except Sale.DoesNotExist:
            response = JsonResponse({"Message": "This sale does not exist "})
            response.status_code = 400
            return response
    elif request.method == "DELETE":
        try:
            sale = Sale.objects.get(id=id)
            sale.delete()
            return JsonResponse({"Deleted": "Sale has been deleted"})
        except Sale.DoesNotExist:
            return JsonResponse(
                {"Message": "Sale doesnot exist, therefor nothing was deleted"}
            )


require_http_methods(["GET"])
def api_list_automobile(request):
    try:
        automobile = AutomobileVO.objects.filter(sold=False)
        return JsonResponse(automobile, encoder=AutomobileVOEncoder, safe=False)
    except AutomobileVO.DoesNotExist:
        response = JsonResponse({"Message": "This sale does not exist "})
        response.status_code = 400
        return response


require_http_methods(["GET"])
def api_salesperson_history(request, employee_id):
    try:
        sales = Sale.objects.filter(salesperson__employee_id=employee_id)

        sales_history = []

        for sale in sales:
            info = {
                "salesperson": sale.salesperson.first_name
                + " "
                + sale.salesperson.last_name,
                "customer": sale.customer.first_name + " " + sale.customer.last_name,
                "automobile": sale.automobile.vin,
                "price": sale.price,
            }
            sales_history.append(info)
        return JsonResponse(sales_history, encoder=SalespersonListEncoder, safe=False)
    except Sale.DoesNotExist:
        response = JsonResponse({"Message": "This Salesperson has no history"})
        response.status_code = 400
        return response
