import { useEffect, useState } from 'react';

function SalespersonList() {
    const [salesperson, setSalespeopleList] = useState([]);

    const fetchData = async () => {
        const listUrl = "http://localhost:8090/api/salespeople/"
        const response = await fetch(listUrl);

        if (response.ok) {
            const data = await response.json();
            setSalespeopleList(data.salesperson);
        }
    };

    useEffect(() => {
        fetchData();
      }, []);
    return(
        <div>
            <div>
                <h1>Salesperson</h1>
            </div>
            <div>
                <table>
                    <thead>
                        <tr>
                            <th>Employee ID</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        {salesperson.map((salesperson)=> {
                            return (
                                <tr key={salesperson.employee_id}>
                                    <td>{salesperson.employee_id}</td>
                                    <td>{salesperson.first_name}</td>
                                    <td>{salesperson.last_name}</td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        </div>
    )
}

export default SalespersonList
