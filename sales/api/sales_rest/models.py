from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)
    import_href = models.URLField()

    def __str__(self):
        return f"{self.vin}, {self.sold}"


class Saleperson(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    employee_id = models.BigIntegerField(unique=True)

    def __str__(self):
        return f"{self.first_name}, {self.last_name}"

    def get_api_url(self):
        return reverse("api_show_salesperson", kwargs={"employee_id": self.employee_id})


class Customer(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    address = models.CharField(max_length=150)
    phone_number = models.PositiveBigIntegerField()

    def __str__(self):
        return f"{self.first_name}, {self.last_name}"

    def get_api_url(self):
        return reverse("api_show_customer", kwargs={"id": self.id})


class Sale(models.Model):
    price = models.IntegerField()
    automobile = models.ForeignKey(
        AutomobileVO, related_name="sale", on_delete=models.CASCADE
    )
    salesperson = models.ForeignKey(
        Saleperson, related_name="sale", on_delete=models.CASCADE
    )
    customer = models.ForeignKey(
        Customer, related_name="sale", on_delete=models.CASCADE
    )

    def __str__(self):
        return f"{self.automobile.vin},{self.price}"

    def get_api_url(self):
        return reverse("api_show_sale", kwargs={"id": self.id})
