import { useEffect, useState } from "react";

function AutomobileList() {
    const [automobile, setAutomobile] = useState([]);

    const fetchData = async () => {
        const listUrl = "	http://localhost:8100/api/automobiles/";
        const response = await fetch(listUrl);

        if (response.ok) {
            const data = await response.json();
            setAutomobile(data.autos)
        }
};

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div>
          <h1>Automobile</h1>
          <table>
            <thead>
              <tr>
                <th>VIN</th>
                <th>Color</th>
                <th>Year</th>
                <th>Model</th>
                <th>Manufacture</th>
                <th>Sold</th>
              </tr>
            </thead>
            <tbody>
              {automobile.map((automobile) => (
                <tr key={automobile.id}>
                  <td>{automobile.vin}</td>
                  <td>{automobile.color}</td>
                  <td>{automobile.year}</td>
                  <td>{automobile.model.name}</td>
                  <td>{automobile.model.manufacturer.name}</td>
                  <td>{String(automobile.sold)}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      );
}

export default AutomobileList
