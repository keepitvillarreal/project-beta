import { useEffect} from "react"
import loadData from ".";

function VehicleModels(props) {
    useEffect(() => {
        loadData();
    }, []);
    if (props.vehicles === undefined) {
        return
    } else {
        return (
            <>
                <div className="container">

                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th>Vehicle Models</th>
                            </tr>
                        </thead>
                        <tbody>
                                <tr>
                                    <th>Name</th>
                                    <th>Picture URL</th>
                                    <th>Manufacturer</th>
                                </tr>
                            {
                                props.vehicles.map((vehicle) => {
                                    return (<>
                                <tr key={vehicle.id}>
                                        <td key={vehicle.name}>{vehicle.name}</td>
                                        <td>{vehicle.picture_url}</td>
                                        <td>{vehicle.manufacturer.name}</td>
                                </tr>
                            </>
                            )
                                }
                            )
                            }
                        </tbody>

                    </table>
                </div>
            </>);

    }
}

export default VehicleModels
