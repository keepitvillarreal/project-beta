import React from 'react';

function TechniciansList(props) {
    if (props.technicians === undefined) {
        return
    } else {
        return (
            <>
                <div className="container">
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th>Technicians</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Employee ID</th>
                            </tr>
                        {
                            props.technicians.map(technician => {
                                return (
                                    <tr key={technician.id}>
                                        <td>{technician.first_name}</td>
                                        <td>{technician.last_name}</td>
                                        <td>{technician.employee_id}</td>
                                    </tr>
                                )
                            }
                            )
                        }
                        </tbody>
                    </table>
                </div>
            </>);

    }
}

export default TechniciansList;
