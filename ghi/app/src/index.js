import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);


async function loadData() {
  const tech_response = await fetch('http://localhost:8080/api/technicians/');
  const data_technicians = await tech_response.json();
  const app_response = await fetch('http://localhost:8080/api/appointments/');
  const data_appointments = await app_response.json();
  const vehicle_response = await fetch("http://localhost:8100/api/models/");
  const data_vehicle = await vehicle_response.json();
  if (app_response.ok && tech_response.ok && vehicle_response.ok) {
    root.render(
      <React.StrictMode>
        <App technicians={data_technicians.technicians} appointments={data_appointments.appointments} vehicles={data_vehicle.models}/>
      </React.StrictMode>
    )
  } else {
    console.error(tech_response);
  }
}
loadData()
export default loadData;
