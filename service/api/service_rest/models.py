from django.db import models
from django.urls import reverse


class Technician(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.CharField(max_length=25, unique=True)

    def get_api_url(self):
        return reverse("api_technician", kwargs={"pk": self.id})


class Appointment(models.Model):
    datetime = models.DateTimeField(auto_now=False, auto_now_add=False)
    reason = models.CharField(max_length=200)
    status = models.CharField(max_length=100)
    vin = models.SmallIntegerField()
    customer = models.CharField(max_length=100)
    is_Vip = models.BooleanField(default=False, null=True)
    technician = models.ForeignKey(
        Technician,
        related_name="technician",
        on_delete=models.CASCADE,
    )

    def get_api_url(self):
        return reverse("api_appointment", kwargs={"pk": self.id})


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=100, unique=True, null=True)
    import_id = models.CharField(max_length=100, unique=True, null=True)
    color = models.CharField(max_length=100, null=True)
    year = models.CharField(max_length=100, null=True)
    vin = models.SmallIntegerField(unique=True)
    sold = models.BooleanField()

    def get_api_url(self):
        return reverse("api_automobile", kwargs={"pk": self.id})
