import React, { useEffect, useState } from 'react';

function AppointmentHistory() {
    const [history, setHistory] = useState([]);
    const fetchData =  async () => {
    const response = await fetch('http://localhost:8080/api/appointments/history/');
    if (response.ok) {
        const data = await response.json();
        setHistory(data)
    }
    }
    useEffect(() => {
        fetchData();
    }, []);
    if (history === undefined) {
        return
    } else {
        return (
            <>
                <div className="container">

                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th>Appointment History</th>
                            </tr>
                        </thead>
                        <tbody>
                                <tr>
                                    <th>Date</th>
                                    <th>VIP?</th>
                                    <th>Reason</th>
                                    <th>Status</th>
                                    <th>VIN</th>
                                    <th>Customer</th>
                                    <th>Technician</th>
                                </tr>
                            {
                                history.map(appointment => {
                                    return (<>
                                <tr key={appointment.id}>
                                        <td>{appointment.datetime}</td>
                                        <td>{String(appointment.is_Vip)}</td>
                                        <td>{appointment.reason}</td>
                                        <td>{appointment.status}</td>
                                        <td>{appointment.vin}</td>
                                        <td>{appointment.customer}</td>
                                        <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                                </tr>
                            </>
                            )
                                }
                            )
                            }
                        </tbody>

                    </table>
                </div>
            </>);

    }

}
export default AppointmentHistory;
